import { axios } from '@/utils/request'

const user = {

    // 添加用户
    saveOrUpdate(parameter,flag) {
        if(flag === 0){
            return axios({
                url: '/api/v1/account/save',
                method: 'post',
                data: parameter
            })
        }else {
            return axios({
                url: '/api/v1/account/update',
                method: 'put',
                data: parameter
            })
        }
    },
    // 获取用户列表
    list(parameter) {
        return axios({
            url: '/api/v1/account/list',
            method: 'get',
            params: parameter
        })
    },
    // 获取用户信息
    getById(id) {
        return axios({
            url: '/api/v1/account/getById/'+id,
            method: 'get'
        })
    },
    // 删除用户
    remove(id) {
        return axios({
            url: '/api/v1/account/delete/'+id,
            method: 'delete'
        })
    },
    // 更新用户状态
    updateUserStatus(id,enabled) {
        return axios({
            url: '/api/v1/account/updateUserStatus/'+id+'/'+enabled,
            method: 'post'
        })
    },
    // 修改密码
    updatePwd(parameter) {
        return axios({
            url: '/api/v1/account/updatePwd',
            method: 'post',
            data: parameter
        })
    },
    // 更改用户头像
    updateAvatar(parameter) {
        return axios({
            url: '/api/v1/account/updateAvatar',
            method: 'post',
            data: parameter
        })
    },
    // 导出用户
    exportUser(parameter) {
        return axios({
            url: '/api/v1/account/export',
            responseType: "blob",
            method: 'post',
            data: parameter
        })
    },
    // 导入用户
    importUser() {
        return axios({
            url: '/api/v1/account/import',
            method: 'post'
        })
    },
    // 根据部门统计用户信息
    getDeptUserCount() {
        return axios({
            url: '/api/v1/account/queryUserCountByDept',
            method: 'get'
        })
    },
    // 统计系统用户总数
    countUser() {
        return axios({
            url: '/api/v1/account/countUser',
            method: 'get'
        })
    },
    // 获取操作日志列表
    getOperatorLogList(parameter) {
        return axios({
            url: '/api/v1/account/queryOperatorLogList',
            method: 'get',
            params: parameter
        })
    },
    // 删除操作日志
    removeOperatorLog(id) {
        return axios({
            url: '/api/v1/account/deleteOperatorLogById/'+id,
            method: 'delete'
        })
    },
    // 获取登录日志列表
    getLoginLogList(parameter) {
        return axios({
            url: '/api/v1/account/queryLoginLogList',
            method: 'get',
            params: parameter
        })
    },
    // 删除登录日志
    removeLoginLog(id) {
        return axios({
            url: '/api/v1/account/deleteLoginLogById/'+id,
            method: 'delete'
        })
    },
    // 记录退出日志
    addLogoutInfo() {
        return axios({
            url: '/api/v1/account/addLogoutInfo/',
            method: 'post'
        })
    },
    // 批量删除操作日志
    bathRemoveOperatorLog(parameter) {
        return axios({
            url: '/api/v1/account/bathDeleteOperatorLogByIds/',
            method: 'delete',
            data: parameter
        })
    },
    // 批量删除操作日志
    bathRemoveLoginLog(parameter) {
        return axios({
            url: '/api/v1/account/bathDeleteLoginLogByIds/',
            method: 'delete',
            data: parameter
        })
    },
}

export default user;
