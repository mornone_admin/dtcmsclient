import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'
Vue.use(VueRouter)
const VueRouterPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (to) {
    return VueRouterPush.call(this, to).catch(err => err)
}
const routes = [
    {
        name: 'login',
        path: '/admin/login',
        component: () => import('@/views/Login.vue')
    },
    {
        path: '/admin/home',
        name: 'home',
        component: Home,
        children: [
            {
                name:'desktop',
                path:'/',
                component: () => import('@/components/DeskTop.vue')
            }
        ]
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
