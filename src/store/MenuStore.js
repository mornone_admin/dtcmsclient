import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
export default {
    state: {
        //当前激活的选项卡
        editableTabsValue: "desktop",
        //tabs选项卡数据
        tabs:[
            {
                title: '工作台',
                name: 'desktop'
            }
        ],
        //左侧菜单数据
        menu_data: [],
        //菜单收缩属性
        isCollapse: false,
        isFlagTitle: false
    },
    mutations: {
        //菜单点击时候调用
        selectMenu(state,val){
            //1.把当前点击的菜单对象，加到tabs里面
            let res = state.tabs.findIndex(item => item.name === val.name);
            //2.判断tabs是否存在，不存在才加入到tabs
            if(res === -1){
                let obj = {};
                obj.title = val.label;
                obj.name = val.name;
                state.tabs.push(obj);
            }
            //设置当前选中对象
            state.editableTabsValue = val.name;
            //设置当前打开的选项卡
            localStorage.setItem('tabList',JSON.stringify(state.tabs));
        },
        //设置当前选中选项卡
        getTabs(state){
            let tabs = localStorage.getItem('tabList');
            if(tabs){
                state.tabs =  JSON.parse(tabs);
            }
        },
        //设置当前激活的选项卡
        setActiveTabs(state,val){
            state.editableTabsValue = val;
        },
        //获取菜单数据和生成路由
        getMenuList(state,router){
            //1.取出菜单数据
            let menuList = localStorage.getItem('menuList');
            //2.设置菜单数据
            if(menuList){
                state.menu_data = JSON.parse(menuList);
            }
            //3.取出路由数据
            let oldrouterList = localStorage.getItem('routerList');
            let routerList = [];
            if(oldrouterList){
                routerList = JSON.parse(oldrouterList);
            }
            //4.动态的生成路由
            //4.1获取原来的路由
            let oldRouter = router.options.routes;
            //遍历后台返回的路由数据，动态生成路由
            routerList.forEach(item =>{
                item.component = () => import(`@/views${item.url}.vue`);
                oldRouter[1].children.push(item);
            })
            //5.添加到现有路由里面
            router.addRoutes(oldRouter);
        },
        //设置收缩菜单属性
        setOpenOrClose(state){
            state.isCollapse = !state.isCollapse;
        },
        setIsFlagTitle(state){
            state.isFlagTitle = !state.isFlagTitle;
        }
    },
    actions: {

    }

}
